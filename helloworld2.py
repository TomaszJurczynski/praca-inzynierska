# -*- coding: utf-8 -*-
# encoding=utf8
from flask import Flask, render_template, url_for, flash, redirect, session, request, logging
from wtforms import Form, StringField, TextAreaField, IntegerField, PasswordField, DateTimeField, validators
from passlib.hash import sha256_crypt
from flask_mysqldb import MySQL
from functools import wraps
from flask_socketio import SocketIO, emit
import sys

konsulacjapacjentpacjent = 1
poradalekarzlekarz = 9

app = Flask(__name__)

app.config[ 'SECRET_KEY' ] = 'jsbcfsbfjefebw237u3gdbdc'
socketio = SocketIO(app)

# Config MSQL
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'root'
app.config['MYSQL_DB'] = 'elekarzdb'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'

# init MSQL
mysql = MySQL(app)


# Index
@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')


# Informacje
@app.route('/informacje')
def informacje():
    return render_template('informacje.html')


# Czy Pacjent jest zalogowany
def is_logged_inPacjent(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_inP' in session:
            return f(*args, **kwargs)
        else:
            flash('Brak dostępu, proszę się zalogować', 'danger')
            return redirect(url_for('zalogujsie'))
    return wrap



# Czy Lekarz jest zalogowany
def is_logged_inLekarz(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_inL' in session:
            return f(*args, **kwargs)
        else:
            flash('Brak dostępu, proszę się zalogować', 'danger')
            return redirect(url_for('zalogujsie'))
    return wrap


# Czy ktos jest zalogowany
def is_logged_in(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_inL' or 'logged_inP' in session:
            return f(*args, **kwargs)
        else:
            flash('Brak dostępu, proszę się zalogować', 'danger')
            return redirect(url_for('zalogujsie'))
    return wrap


# Twoje konwersacje
@app.route('/twojekonwersacje')
@is_logged_in
def twojekonwersacje():
    resultporada2 = 0
    resultporada = 0
    poradalekarza = False
    if int(request.args.get('identyfikator')) > 0:
        identyfikator = int(request.args.get('identyfikator'))
    else:
        return render_template('twojekonwersacje.html')

    cur = mysql.connection.cursor()

    if session['logged_inL']:
        # result1 = cur.execute("SELECT * FROM konsultacje JOIN pacjent WHERE LekarzID = %s AND typ_konsultacji = %s", (identyfikator, konsulacjapacjentpacjent))
        result1 = cur.execute("SELECT * FROM konsultacje JOIN pacjent ON konsultacje.PacjentID = pacjent.id WHERE konsultacje.LekarzID = %s AND konsultacje.typ_konsultacji = %s", (identyfikator, konsulacjapacjentpacjent))
        kons = cur.fetchall()
        print(kons)
        resultporada = cur.execute("SELECT * FROM konsultacje WHERE PacjentID = %s AND typ_konsultacji = %s", (identyfikator, poradalekarzlekarz))
        if resultporada > 0:
            poradalekarza = cur.fetchall()
        resultporada2 = cur.execute("SELECT * FROM konsultacje WHERE LekarzID = %s AND typ_konsultacji = %s", (identyfikator, poradalekarzlekarz))
        if resultporada2 > 0:
            poradalekarza = cur.fetchall()
        result2 = 0
        if resultporada == 0 and resultporada2 == 0 and result1 == 0:  # brak konsultacji
            msg = 'Nie zanleziono żadnych konsultacji'
            return render_template('twojekonwersacje.html', msg=msg)

    elif session['logged_inP']:
        result2 = cur.execute("SELECT * FROM konsultacje WHERE PacjentID = %s AND typ_konsultacji = %s", (identyfikator, konsulacjapacjentpacjent))
        kons = cur.fetchall()
        print("TUTAJ KONS", kons)
        result1 = 0
    else:
        flash('Brak dostępu, proszę się zalogować', 'danger')
        return redirect(url_for('zalogujsie'))

    if result1 > 0 or result2 > 0 or resultporada > 0 or resultporada2 > 0:
        if session['logged_inL'] and poradalekarza:
            return render_template('twojekonwersacje.html', konsultacje=kons, poradalekarza=poradalekarza)
        else:
            return render_template('twojekonwersacje.html', konsultacje=kons)
    else:
        msg = 'Brak konsultacjii'
        return render_template('twojekonwersacje.html', msg=msg)


# Twoje porady
@app.route('/twojeporady')
@is_logged_in
def twojeporady():
    resultporada2 = 0
    resultporada = 0
    poradalekarza = False
    if int(request.args.get('identyfikator')) > 0:
        identyfikator = int(request.args.get('identyfikator'))
    else:
        return render_template('twojeporady.html')

    cur = mysql.connection.cursor()

    if session['logged_inL']:
        # result1 = cur.execute("SELECT * FROM konsultacje WHERE LekarzID = %s AND typ_konsultacji = %s", (identyfikator, konsulacjapacjentpacjent))
        # kons = cur.fetchall()
        resultporada = cur.execute("SELECT * FROM konsultacje WHERE PacjentID = %s AND typ_konsultacji = %s", (identyfikator, poradalekarzlekarz))
        if resultporada > 0:
            poradalekarza = cur.fetchall()
        resultporada2 = cur.execute("SELECT * FROM konsultacje WHERE LekarzID = %s AND typ_konsultacji = %s", (identyfikator, poradalekarzlekarz))
        if resultporada2 > 0:
            poradalekarza = cur.fetchall()
        # if resultporada == 0 and resultporada2 == 0:  # brak konsultacji
        #     msg = 'Nie zanleziono żadnych konsultacji'
        #     return render_template('twojeporady.html', msg=msg)
    else:
        flash('Brak dostępu, proszę się zalogować', 'danger')
        return redirect(url_for('zalogujsie'))

    if resultporada > 0 or resultporada2 > 0:
        if session['logged_inL'] and poradalekarzlekarz:
            return render_template('twojeporady.html', poradalekarza=poradalekarza)
    else:
        msg = 'Brak porad'
        return render_template('twojeporady.html', msg=msg)


# Zaloguj sie
@app.route('/zalogujsie', methods=["GET", "POST"])
def zalogujsie():
    if request.method == 'POST':
        login = request.form['login']
        haslo_kandydat = request.form['haslo']
        cur = mysql.connection.cursor()
        global resultL
        global resultP
        # users pacjent/lekarz
        if cur.execute("SELECT * FROM lekarz WHERE login = %s", [login]) > 0:
            resultL = cur.execute("SELECT * FROM lekarz WHERE login = %s", [login])
            resultP = 0
        elif cur.execute("SELECT * FROM pacjent WHERE login = %s", [login]) > 0:
            resultP = cur.execute("SELECT * FROM pacjent WHERE login = %s", [login])
            resultL = 0
        else:
            error = 'Dane niepoprawne'
            return render_template('zalogujsie.html', error=error)

        if resultP > 0:
            data = cur.fetchone()
            haslo = data['haslo']
            imie = data['imie']
            nazwisko = data['nazwisko']
            id = data['id']
            if sha256_crypt.verify(haslo_kandydat, haslo):
                # app.logger.info('Haslo poprawne')
                session['logged_inP'] = True
                session['logged_inL'] = False
                session['login'] = login
                session['imie'] = imie
                session['nazwisko'] = nazwisko
                session['id'] = id
                flash('Zostałeś poprawnie zalogowany', 'success')
                return redirect(url_for('pacjent'))
            else:
                error = 'Hasło niepoprawne'
                return render_template('zalogujsie.html', error=error)
            cur.close()
        elif resultL > 0:
            data = cur.fetchone()
            haslo = data['haslo']
            imie = data['imie']
            nazwisko = data['nazwisko']
            specjalizacja = data['specjalizacja']
            id = data['id']
            if sha256_crypt.verify(haslo_kandydat, haslo):
                # app.logger.info('Haslo poprawne')
                session['logged_inL'] = True
                session['logged_inP'] = False
                session['login'] = login
                session['imie'] = imie
                session['nazwisko'] = nazwisko
                session['id'] = id
                session['specjalizacja'] = specjalizacja
                flash('Zostałeś poprawnie zalogowany', 'success')
                return redirect(url_for('lekarz'))
            else:
                error = 'Hasło niepoprawne'
                return render_template('zalogujsie.html', error=error)
            cur.close()
        else:
            error = 'Nie ma takiego użytkownika'
            return render_template('zalogujsie.html', error=error)
    return render_template('zalogujsie.html')


# Profil lekarza
@app.route('/profillekarza')
def profillekarza():
    userid = int(request.args.get('userid'))
    cur = mysql.connection.cursor()
    result = cur.execute("SELECT * FROM lekarz WHERE id = %s", [userid])
    danelekarza = cur.fetchone()
    if result > 0:
        return render_template('profillekarza.html', danelekarza=danelekarza)
    else:
        msg = 'Nie zanleziono lekarzy'
        return render_template('profillekarza.html', msg=msg)


# Register Form Class - Pacjent
class RegisterFormP(Form):
    imie = StringField('Imie', [validators.Length(min=1, max=50, message="Pole musi zawierać od 1 do 50 znaków.")])
    nazwisko = StringField('Nazwisko', [validators.Length(min=1, max=50, message="Pole musi zawierać od 1 do 50 znaków.")])
    email = StringField('E-mail', [validators.Email(message='Proszę poprawnie wprowadzić E-mail')])
    nrtelefonu = StringField('Numer telefonu', [validators.Length(min=8, message='Numer telefonu musi zawierać od 8 do 15 cyfr')])
    pesel = StringField('PESEL', [validators.Length(min=11, message='PESEL musi zawierać 11 cyfr')])
    login = StringField('Login', [validators.Length(min=5, max=25, message="Login musi zawierać od 5 do 50 znaków")])
    haslo = PasswordField('Haslo', [validators.DataRequired(message="Proszę wprowadzić hasło"),
                                    validators.EqualTo('potwierdz', message='Hasla musza byc takie same')])
    potwierdz = PasswordField('Potwierdz haslo')


# Register Form Class - Lekarz
class RegisterFormL(Form):
    imie = StringField('Imie', [validators.Length(min=1, max=50, message="Pole musi zawierać od 1 do 50 znaków.")])
    nazwisko = StringField('Nazwisko', [validators.Length(min=1, max=50, message="Pole musi zawierać od 1 do 50 znaków.")])
    email = StringField('E-mail', [validators.Email(message='Proszę poprawnie wprowadzić E-mail')])
    nrtelefonu = StringField('Numer telefonu', [validators.Length(min=8, message='Numer telefonu musi zawierać od 8 do 15 cyfr')])
    pesel = StringField('PESEL', [validators.Length(min=11, message='PESEL musi zawierać 11 cyfr')])
    login = StringField('Login', [validators.Length(min=5, max=50, message="Login musi zawierać od 5 do 50 znaków")])
    haslo = PasswordField('Haslo', [validators.DataRequired(message="Proszę wprowadzić hasło"),
                                    validators.EqualTo('potwierdz', message='Hasla musza byc takie same')])
    potwierdz = PasswordField('Potwierdz haslo')
    specjalizacja = StringField('Specjalizacja', [validators.Length(min=1, max=50, message="Proszę wprowadzić specjalizację")])
    nrPrawaWykonywaniaZawodu = StringField('Numer prawa wykonywania zawodu', [validators.Length(min=7, message="Numer wykonywania zawodu musi zawierać 7 cyfr")])



class SettingsForm(Form):
    email = StringField('E-mail', [validators.Email(message='Proszę poprawnie wprowadzić E-mail')])
    nrtelefonu = StringField('Numer telefonu', [validators.Length(min=8, message='Numer telefonu musi zawierać od 8 do 15 cyfr')])
    haslo = PasswordField('Haslo', [validators.DataRequired(message="Proszę wprowadzić hasło"),
                                    validators.EqualTo('potwierdz', message='Hasla musza byc takie same')])
    potwierdz = PasswordField('Potwierdz haslo')


# Wiadomosc Form Class
class WiadomoscForm(Form):
    zawartosc = TextAreaField('Zawarość wiadomości', [validators.Length(min=1)])
    # nadawca = StringField('Nadawca')
    # data = DateTimeField('Data')


# Konsultacje Form Class
class KonsultacjeForm(Form):
    typ_konsultacji = StringField('Typ Konwersacji')
    temat = StringField('Temat konsultacji', [validators.Length(min=5)])


# Zaloz konto
@app.route('/zalozkonto')
def zalozkonto():
    return render_template('zalozkonto.html')


@app.route('/ustawieniaprofilu', methods=["GET", "POST"])
@is_logged_in
def ustawieniaprofilu():

    form = SettingsForm(request.form)
    identyfikator = request.args.get('identyfikator')
    if request.method == "POST" and form.validate():
        email = form.email.data
        nrtelefonu = form.nrtelefonu.data
        haslo = sha256_crypt.encrypt(str(form.haslo.data))
        cur = mysql.connection.cursor()
        if session['logged_inL']:
            cur.execute("UPDATE lekarz SET email = %s, nr_telefonu = %s, haslo = %s WHERE id = %s",
                        (email, nrtelefonu, haslo, identyfikator))
        elif session['logged_inP']:
            cur.execute("UPDATE pacjent SET email = %s, nr_telefonu = %s, haslo = %s WHERE id = %s",
                        (email, nrtelefonu, haslo, identyfikator))
        else:
            flash('Brak dostępu, proszę się zalogować', 'danger')
            return redirect(url_for('zalogujsie'))

        mysql.connection.commit()
        cur.close()

        flash('Zapisano nowe ustawienia', 'success')
        return redirect(url_for('lekarz'))
    return render_template('ustawieniaprofilu.html', form=form)


# Konsultacja
@app.route('/konsultacja', methods=["GET", "POST"])
@is_logged_in
def konsultacja():
    danelekarza = 0
    form = KonsultacjeForm(request.form)
    userid = request.args.get('userid')
    pacjent = request.args.get('pacjent')
    # if pacjent == session['id'] and session['logged_inL']:
    #     mamy porade
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM lekarz WHERE id = %s", [userid])
    if userid != pacjent:
        danelekarza = cur.fetchone()
    else:
        msg = "Nie możesz prowadzić konwersacji z samym sobą"
        # return redirect(url_for('konsultacja', msg=msg))
        return render_template('konsultacja.html', msg=msg, danelekarza=danelekarza)

    if request.method == 'POST' and form.validate():
        typkonwersacji = form.typ_konsultacji.data
        tematkonsultacji = form.temat.data

        cur.execute("INSERT INTO konsultacje(LekarzID, PacjentID, typ_konsultacji, temat) VALUES(%s ,%s, %s, %s)", (danelekarza['id'], pacjent, typkonwersacji, tematkonsultacji))

        mysql.connection.commit()

        cur.close()
        flash('Konsultacja została założona', 'success')
        redirect(url_for('konsultacja'))
        # return render_template('zalozkontoP.html', form=form)

    return render_template('konsultacja.html', form=form, danelekarza=danelekarza)


# Rozmowa
@app.route('/rozmowa', methods=["GET", "POST"])
@is_logged_in
def rozmowa():
    form = WiadomoscForm(request.form)
    nadawcaid = int(request.args.get('nadawcaid'))
    odbiorcaid = int(request.args.get('odbiorcaid'))
    # porada = int(request.args.get('porada'))
    konsultacja = int(request.args.get('konsultacja'))
    print("nadawca: ", nadawcaid)
    print("odbiorca: ", odbiorcaid)
    print("kondiuulk: ", type(konsultacja))
    print(session['id'], nadawcaid)

    cur = mysql.connection.cursor()

    if session['id'] == nadawcaid and session['logged_inP']:
        resultK = cur.execute("SELECT * FROM konsultacje WHERE LekarzID = %s AND PacjentID = %s AND typ_konsultacji = %s",
                              (nadawcaid, odbiorcaid, poradalekarzlekarz))
        if resultK == 0:
            resultK = cur.execute("SELECT * FROM konsultacje WHERE LekarzID = %s AND PacjentID = %s AND typ_konsultacji = %s",
                (odbiorcaid, nadawcaid, poradalekarzlekarz))
    else:
        resultK = cur.execute("SELECT * FROM konsultacje WHERE LekarzID = %s AND PacjentID = %s",
                              (odbiorcaid, nadawcaid))


    # if resultK > 0 and porada == poradalekarzlekarz and session['id'] != nadawcaid:
    #     cur.execute("SELECT * FROM konsultacje WHERE LekarzID = %s AND PacjentID = %s AND typ_konsultacji = %s",
    #                 (odbiorcaid, nadawcaid, poradalekarzlekarz))
    #     konsultacjaID = cur.fetchone()
    #     idkonsultacji = konsultacjaID['id']
    #     konsultacja = 0
    if resultK > 0 and konsultacja == konsulacjapacjentpacjent:
        cur.execute("SELECT * FROM konsultacje WHERE LekarzID = %s AND PacjentID = %s AND typ_konsultacji = %s",
                    (odbiorcaid, nadawcaid, konsulacjapacjentpacjent))
        konsultacjaID = cur.fetchone()
        idkonsultacji = konsultacjaID['id']


        # porada = 0
    # elif resultK > 0 and porada == poradalekarzlekarz and session['id'] == nadawcaid:
    #     temp = cur.execute("SELECT * FROM konsultacje WHERE LekarzID = %s AND PacjentID = %s AND typ_konsultacji = %s",
    #                 (nadawcaid, odbiorcaid, poradalekarzlekarz))
    #     if temp == 0:
    #         temp = cur.execute("SELECT * FROM konsultacje WHERE LekarzID = %s AND PacjentID = %s AND typ_konsultacji = %s",
    #             (odbiorcaid, nadawcaid, poradalekarzlekarz))
    #         konsultacjaID = cur.fetchone()
    #         idkonsultacji = konsultacjaID['id']
    #         konsultacja = 0
    #     else:
    #         konsultacjaID = cur.fetchone()
    #         idkonsultacji = konsultacjaID['id']
    #         konsultacja = 0
    #     print("tu jestem")
    else:
        print("jakis przypadek")
        idkonsultacji = -1
        konsultacjaID = 0

    if request.method == 'POST' and form.validate():
        zawartosc = form.zawartosc.data
        cur.execute("INSERT INTO wiadomosci(KonsultacjaID, zawartosc, nadawca) VALUES(%s, %s, %s)", (konsultacjaID['id'], zawartosc, session['login']))
        mysql.connection.commit()
        # cur.close()

    result = cur.execute("SELECT * FROM wiadomosci WHERE KonsultacjaID = %s", [idkonsultacji])
    if result > 0:
        wiadomosci = cur.fetchall()
        return render_template('rozmowa.html', wiadomosci=wiadomosci, form=form)
    else:
        msg = 'Brak wiadomości w tej konwersacji'
        return render_template('rozmowa.html', msg=msg, form=form)


# Odczytaj wiadomosc
@app.route('/odczytajwiadomosc', methods=["GET", "POST"])
@is_logged_inPacjent
def odczytajwiadomosc():
    cur = mysql.connection.cursor()
    result = cur.execute("SELECT * FROM wiadomosci")
    wiadomosci = cur.fetchall()

    if result > 0:
        return render_template('odczytajwiadomosc.html', wiadomosci=wiadomosci)
    else:
        msg = 'Nie zanleziono żadnych wiadomości'
        return render_template('odczytajwiadomosc.html', msg=msg)


# Wybierz Lekarza
@app.route('/wybierzlekarza', methods=["GET", "POST"])
@is_logged_in
def wybierzlekarza():
    cur = mysql.connection.cursor()
    result = cur.execute("SELECT * FROM dane_kontaktowe_lekarz")
    danekontaktowe = cur.fetchall()

    if result > 0:
        return render_template('wybierzlekarza.html', danekontaktowe=danekontaktowe)
    else:
        msg = 'Nie zanleziono lekarzy'
        return render_template('wybierzlekarza.html', msg=msg)


# Zaloz konto jako Pacjent
@app.route('/zalozkontoPacjent', methods=["GET", "POST"])
def zalozkontoP():
    form = RegisterFormP(request.form)
    if request.method == "POST" and form.validate():
        imie = form.imie.data
        nazwisko = form.nazwisko.data
        email = form.email.data
        nrtelefonu = form.nrtelefonu.data
        pesel = form.pesel.data
        login = form.login.data
        haslo = sha256_crypt.encrypt(str(form.haslo.data))
        cur = mysql.connection.cursor()

        cur.execute("INSERT INTO pacjent(imie, nazwisko, email, nr_telefonu, pesel, login, haslo)"
                    " VALUES(%s, %s, %s, %s, %s, %s, %s)", (imie, nazwisko, email, nrtelefonu, pesel, login, haslo))

        mysql.connection.commit()
        cur.close()
        flash('Konto zostało założone', 'success')
        redirect(url_for('zalogujsie'))
        redirect(url_for('index'))
        # return render_template('zalozkontoP.html', form=form)
    return render_template('zalozkontoP.html', form=form)


# Zaloz konto jako Lekarz
@app.route('/zalozkontoLekarz', methods=['GET', "POST"])
def zalozkontoL():
    form = RegisterFormL(request.form)
    if request.method == "POST" and form.validate():
        imie = form.imie.data
        nazwisko = form.nazwisko.data
        email = form.email.data
        nrtelefonu = form.nrtelefonu.data
        pesel = form.pesel.data
        specjalizacja = form.specjalizacja.data
        nrprawawykonywaniazawodu = form.nrPrawaWykonywaniaZawodu.data
        login = form.login.data
        haslo = sha256_crypt.encrypt(str(form.haslo.data))
        cur = mysql.connection.cursor()

        cur.execute(
            "INSERT INTO lekarz(imie, nazwisko, email, nr_telefonu, pesel, specjalizacja, nr_wykonywania_zawodu, login, haslo)"
            " VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s)",
            (imie, nazwisko, email, nrtelefonu, pesel, specjalizacja, nrprawawykonywaniazawodu, login, haslo))

        mysql.connection.commit()
        cur.close()
        flash('Konto zostało założone', 'success')
        redirect(url_for('zalogujsie'))
        redirect(url_for('index'))
    return render_template('zalozkontoL.html', form=form)


# Wyloguj sie
@app.route('/wylogujL')
@is_logged_inLekarz
def wylogujL():
    session.clear()
    flash('Zastałeś poprawnie wylogowany', 'success')
    return redirect(url_for('index'))

# Wyloguj sie
@app.route('/wylogujP')
@is_logged_inPacjent
def wylogujP():
    session.clear()
    flash('Zastałeś poprawnie wylogowany', 'success')
    return redirect(url_for('index'))


# Dashboard Pacjent
@app.route('/pacjent')
@is_logged_inPacjent
def pacjent():
    return render_template('pacjent.html')


# Dashboard Lekarz
@app.route('/lekarz')
@is_logged_inLekarz
def lekarz():
    return render_template('lekarz.html')


# Chat
@app.route('/chat')
def chat():
    return render_template('chat.html')


# Message recived
def messageRecived():
  print('message was received!!!')


# socketio messages
@socketio.on('my event')
def handle_my_custom_event(json):
  print('recived my event: ' + str(json))
  socketio.emit('my response', json, callback=messageRecived)


# Lista lekarzy
@app.route('/listalekarzy')
def listalekarzy():
    cur = mysql.connection.cursor()
    result = cur.execute("SELECT * FROM lekarz")
    danekontaktowe = cur.fetchall()

    if result > 0:
        return render_template('listalekarzy.html', danekontaktowe=danekontaktowe)
    else:
        msg = 'Nie zanleziono lekarzy'
        return render_template('listalekarzy.html', msg=msg)


if __name__ == '__main__':
    # app.secret_key = 'secret123'
    app.run(debug=True)
    socketio.run(app, debug=True)
